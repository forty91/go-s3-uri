package common

import (
	"fmt"
	"time"

	"gitlab.com/miatel/go/log"
)

func TimeTrack(start time.Time, event string) {
	elapsed := time.Since(start)
	log.Debug(fmt.Errorf("%s took %s", event, elapsed))
}
