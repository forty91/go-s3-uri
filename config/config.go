package config

import (
	"os"
	"path"

	"github.com/go-ini/ini"

	"go-s3-uri/server"
)

type Config struct {
	General  General               `ini:"general"`
	//S3       s3.Config             `ini:"s3"`
	Server   server.Config         `ini:"server"`
}

type General struct {
	LogLevel string `ini:"log_level"`
	LogDest  string `ini:"log_destination"`
	LogTag   string `ini:"log_tag"`
	PidFile  string `ini:"pidfile"`
}

func (cfg *Config) Validate() (err error) {
	//if err = cfg.S3.Validate(); err != nil {
	//	return
	//}
	if err = cfg.Server.Validate(); err != nil {
		return
	}

	return
}

func Parse(fileName string) (*Config, error) {
	var (
		err     error
		iniFile *ini.File
	)

	if iniFile, err = ini.Load(fileName); err != nil {
		return nil, err
	}

	cfg := &Config{
		General: General{
			LogLevel: "debug",
			LogTag:   path.Base(os.Args[0]),
			PidFile:  "/var/run/" + path.Base(os.Args[0]) + ".pid",
		},
	}

	if err = iniFile.MapTo(cfg); err != nil {
		return nil, err
	}

	return cfg, cfg.Validate()
}
