package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/miatel/go/log"
	"gitlab.com/miatel/go/pidfile"

	"go-s3-uri/config"
	"go-s3-uri/server"
)

const defaultConfig = "config.ini"

func main() {
	os.Exit(realMain())
}

func realMain() int {
	var (
		err     error
		cfgFile string
		cfg     *config.Config
	)

	if len(os.Args) < 2 || os.Args[1] == "" {
		cfgFile = defaultConfig
	} else {
		cfgFile = os.Args[1]
	}

	if cfg, err = config.Parse(cfgFile); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Load config error: %s", err)
		return 1
	}

	if err = log.Init(cfg.General.LogDest, cfg.General.LogTag, cfg.General.LogLevel); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Init logger error: %s", err)
		return 1
	}
	defer log.Close()

	if err = pidfile.Write(cfg.General.PidFile); err != nil {
		log.Error("Create PID file error: ", err)
		return 1
	}
	defer pidfile.Unlink(cfg.General.PidFile)

	if err = server.Init(&cfg.Server); err != nil {
		log.Error("Initialize http server error: ", err)
		return 1
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	server.Run(ctx, cancel)
	defer server.Shutdown()

	signalEvents := make(chan os.Signal, 1)
	signal.Notify(signalEvents, syscall.SIGINT, syscall.SIGHUP, syscall.SIGTERM)

	select {
	case s := <-signalEvents:
		log.Info(fmt.Sprintf("Caught signal %v: terminating", s))
	case <-ctx.Done():
		return 1
	}

	return 0
}

//func main() {
//	router := mux.NewRouter()
//	router.Schemes("s3")
//	//router.Use(handlers.ProxyHeaders)
//	router.Use(AlwaysS3)
//	router.HandleFunc("/products/{id:[0-9]+}", productsHandler)
//	http.Handle("/", router)
//
//	fmt.Println("Server is listening...")
//	fmt.Println(http.ListenAndServe(":8080", nil))
//}
//
//type Server struct{
//	http.Server
//	//inShutdown int32
//}
//
//func (srv *Server) ListenAndServeS3() error {
//	if srv.shuttingDown() {
//		return ErrServerClosed
//	}
//	addr := srv.Addr
//	if addr == "" {
//		addr = ":s3"
//	}
//	ln, err := net.Listen("tcp", addr)
//	if err != nil {
//		return err
//	}
//	return srv.Serve(ln)
//}
//
//func AlwaysS3(next http.Handler) http.Handler {
//	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		r.URL.Scheme = "s3"
//
//		next.ServeHTTP(w, r)
//	})
//}
//
//func productsHandler(w http.ResponseWriter, r *http.Request) {
//	vars := mux.Vars(r)
//	id := vars["id"]
//	response := fmt.Sprintf("Product %s", id)
//	fmt.Fprint(w, response)
//}
