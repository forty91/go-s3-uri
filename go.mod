module go-s3-uri

go 1.14

require (
	github.com/go-ini/ini v1.57.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/minio/minio-go/v6 v6.0.56
	gitlab.com/miatel/go/log v1.0.2
	gitlab.com/miatel/go/pidfile v1.0.1
)
