package server

import (
	"fmt"
	"net/http"
	"net/url"
)

func JsonError(writer http.ResponseWriter, err interface{}, code int) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(code)
	fmt.Fprintf(writer, `{"error":"%v"}`, err)
}

func JsonIdUrl(writer http.ResponseWriter, id uint64, url *url.URL, code int) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(code)
	fmt.Fprintf(writer, `{"id":"%d", "url": "%s"}`, id, url.String())
}
