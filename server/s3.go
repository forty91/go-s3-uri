package server

import (
	"fmt"
	"net/url"
	"time"

	"github.com/minio/minio-go/v6"

	"go-s3-uri/common"
)

func getRedirectUrl(s3Host, s3Port, accessKey, secretKey string, ssl bool, bucket, key string) (redirectURL *url.URL, err error) {
	defer common.TimeTrack(time.Now(), "getRedirectUrl")

	var (
		client *minio.Client
	)

	if client, err = minio.New(fmt.Sprintf("%s:%s", s3Host, s3Port), accessKey, secretKey, ssl); err != nil {
		return
	}
	if redirectURL, err = client.PresignedGetObject(bucket, key, time.Duration(30)*time.Second, nil); err != nil {
		return
	}

	return
}
