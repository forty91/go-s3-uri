package server

import (
	"errors"
)

type Config struct {
	Host string `ini:"host"`
	Port int    `ini:"port"`
}

var (
	errHost = errors.New("server 'host' not defined")
	errPort = errors.New("server 'port' invalid")
)

func (c *Config) Validate() error {
	const (
		MinFileSize int64 = 1
		MaxFileSize int64 = 20 * 1024 * 1024
	)
	if c.Host == "" {
		return errHost
	}
	if c.Port < 0 || c.Port > 65536 {
		return errPort
	}
	return nil
}
