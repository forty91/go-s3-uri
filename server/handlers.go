package server

import (
	"net/http"
	"net/url"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/miatel/go/log"
)

func GetHandler(writer http.ResponseWriter, request *http.Request, params httprouter.Params) {
	var (
		err         error
		redirectURL *url.URL
		accessKey   string
		secretKey   string
		s3Host      string
		s3Port      string
		bucket      string
		key         string
		ssl         bool
	)

	urlQueryValues := request.URL.Query()
	accessKey = urlQueryValues.Get("access")
	secretKey = urlQueryValues.Get("secret")
	s3Host = urlQueryValues.Get("host")
	s3Port = urlQueryValues.Get("port")
	bucket = urlQueryValues.Get("bucket")
	key = urlQueryValues.Get("key")
	if ssl, err = strconv.ParseBool(urlQueryValues.Get("ssl")); err != nil {
		JsonError(writer, "Incorrect ssl", http.StatusBadRequest)
	}

	if redirectURL, err = getRedirectUrl(s3Host, s3Port, accessKey, secretKey, ssl, bucket, key); err != nil {
		JsonError(writer, err, http.StatusBadRequest)
		return
	}

	log.Info(redirectURL.String())
	http.Redirect(writer, request, redirectURL.String(), http.StatusTemporaryRedirect)
}
